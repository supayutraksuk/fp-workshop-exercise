document.getElementById('fileinput').addEventListener('change', readFileFromEvent, false)

function readFileFromEvent (event) {
	var file = event.target.files[0]
	var r = new FileReader()
	r.onload = updateContent
	r.readAsBinaryString(file)
}

function logFileLoad(event) {
	var content = event.target.result
	console.log(content)
}

function updateContent(event) {
	// logFileLoad(event)
	setContent(parseMarkDown(event.target.result))
}

function setContent(content) {
	document.getElementById("main").innerHTML = content
}

function parseMarkDown (content) {
	var match = R.match(R.__, R.__)
	var matchHead = R.curry(match(/^#+/g))
	var insertParagraghAndHeader = line => {
		return (R.trim(line)[0] != '#')? '<p>'+ line +'</p>': 
		matchHead(R.trim(line)) != []? R.replace(
										/^#+/g,
										'<h'+ R.length(matchHead(R.trim(line))[0]) +'>',
										R.trim(line)
									) + '</h'+ R.length(matchHead(R.trim(line))[0])+'>': ''
	}
	var contentList = R.split(/^$/gm, content)
	var contentWithPandH = R.map(insertParagraghAndHeader, contentList)
	var insertBold = x => {
		var line = x
		var matchStar = match(/\*[^\*]+\*/g, line)
		var replaceLine = boldText => {
			line = R.replace(/\*[^\*]+\*/, '<b>'+ boldText.slice(1, -1) + '</b>', line)
		}
		R.forEach(replaceLine, matchStar)
		return line
	}
	var mergeContent = R.reduce(R.concat, "", R.map(insertBold, contentWithPandH))
	content = R.replace(/<br><br><p>/gm, '<p>', mergeContent)
	return content
}
